import React from 'react';
import styled from 'styled-components'
// import christmastree from "./christmas-tree.png"

function App() {
  return (
    <Background>
      <Wrapper>
        <Left>
          <Heading>Merry Christmas!</Heading>
        </Left>
        <Right>
          <Image src="https://gallery.yopriceville.com/var/albums/Free-Clipart-Pictures/Christmas-PNG/Transparent_Christmas_Tree_with_Ornaments_PNG_Picture.png?m=1434276836" alt="Logo" />
        </Right>
      </Wrapper>
    </Background>
  );
}

const Background = styled.div`
  height: 100vh;
  width: 100vw;
  display: flex;
  background-image: url('https://icdn2.themanual.com/image/themanual/dark-night-sky.jpg');
`

const Heading = styled.h1`
  color: white;
`

const Wrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-evenly;
  width: 60vw;
  margin-left: 20vw;
`

const Image = styled.img`
  height: 80%;
  /* width: auto; */
`

const Left = styled.div`
  display: flex;
  flex-direction: column;
  height: 100vh;
  justify-content: center;
`

const Right = styled.div`
  display: flex;
  flex-direction: column;
  height: 100vh;
  justify-content: center;
`

export default App;
